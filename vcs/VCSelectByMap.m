//
//  VCSelectByMap.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCSelectByMap.h"

@interface VCSelectByMap ()

@end

@implementation VCSelectByMap

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnCancel layer] setCornerRadius:5];
    [btnCancel setClipsToBounds:YES];
    [[btnDone layer] setCornerRadius:5];
    [btnDone setClipsToBounds:YES];
    [[vListHolder layer] setCornerRadius:5];
    [vListHolder setClipsToBounds:YES];
    span.latitudeDelta = 0.112872;
    span.longitudeDelta = 0.109863;
    UILongPressGestureRecognizer *pressMap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapLongPress:)];
    [mkm addGestureRecognizer:pressMap];
}

- (void)viewDidAppear:(BOOL)animated{
    BOOL msgShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"mapMsgShown"];
    if(!msgShown){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"mapMsgShown"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Select Location" message:@"Long Press the map to drop a pin on your selected location. Tap the pin to view and choose an address." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if(!userLocationShown){
        userLocationShown = YES;
        CLLocationCoordinate2D location;
        if(!_byZip){
            location.latitude = userLocation.coordinate.latitude;
            location.longitude = userLocation.coordinate.longitude;
            MKCoordinateRegion region;
            region.span = span;
            region.center = location;
            [mkm setRegion:region animated:NO];
        }
    }
}

- (void)searchByCityStateOrZip:(NSString *)cityStateOrZip{
    NSString *formatted = [cityStateOrZip stringByReplacingOccurrencesOfString:@"," withString:@""];
    formatted = [formatted stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    formatted = [formatted stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", formatted]];
    NSData *result = [NSData dataWithContentsOfURL:url];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
    NSArray *arr = [dic objectForKey:@"results"];
    NSDictionary *dicAddress = [arr objectAtIndex:0];
    NSString *address = [dicAddress objectForKey:@"formatted_address"];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
        if(error){
            [self showLocationError:error];
            return;
        }
        if([placemarks count] > 0){
            MKPlacemark *placemark = [placemarks objectAtIndex:0];
            currRegion.span = span;
            currRegion.center = placemark.location.coordinate;
            [mkm setRegion:currRegion animated:NO];
            [barSearch setText:@"tires"];
            [self performSelector:@selector(searchBarSearchButtonClicked:) withObject:barSearch];
        }
    }];
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    currRegion = [mapView region];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    [vListHolder setHidden:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    currRegion.span = span;
    currRegion.span = span;
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    [request setNaturalLanguageQuery:[searchBar text]];
    [request setRegion:currRegion];
    MKLocalSearchCompletionHandler completionHandler = ^(MKLocalSearchResponse *response, NSError *error){
        if(error){
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Location Error" message:[NSString stringWithFormat:@"%@ Please try again later.", [error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }else{
            [mkm removeAnnotations:[mkm annotations]];
            [vListHolder setHidden:YES];
            annotations = [[NSMutableArray alloc] init];
            BOOL firstItem = YES;
            for(MKMapItem *item in [response mapItems]){
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                [annotation setCoordinate:[[item placemark] coordinate]];
                [annotation setTitle:[item name]];
                [annotation setSubtitle:[[item placemark] title]];
                [mkm addAnnotation:annotation];
                if(firstItem){
                    firstItem = NO;
                    currRegion.span = span;
                    currRegion.center = [[item placemark] coordinate];
                    [mkm setRegion:currRegion animated:NO];
                    if([[response mapItems] count] > 1){
                        [vListHolder setHidden:NO];
                    }
                }else{
                    [annotations addObject:annotation];
                }
            }
            [tbl reloadData];
        }
    };
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    [localSearch startWithCompletionHandler:completionHandler];
}

-(void)handleMapLongPress:(UILongPressGestureRecognizer *)sender{
    [mkm removeAnnotations:[mkm annotations]];
    annotations = nil;
    [vListHolder setHidden:YES];
    [tbl reloadData];
    CGPoint point = [sender locationInView:mkm];
    CLLocationCoordinate2D location2D = [mkm convertPoint:point toCoordinateFromView:mkm];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location2D.latitude longitude:location2D.longitude];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
        if(error){
            [self showLocationError:error];
            return;
        }
        NSDictionary *dicAddress = [[placemarks objectAtIndex:0] addressDictionary];
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:location2D];
        if(![[dicAddress objectForKey:@"Street"] isEqualToString:[dicAddress objectForKey:@"Name"]]){
            [annotation setTitle:[dicAddress objectForKey:@"Name"]];
        }else{
            [annotation setTitle:@"Selected Location"];
        }
        [annotation setSubtitle:[NSString stringWithFormat:@"%@, %@, %@  %@, %@", [dicAddress objectForKey:@"Street"], [dicAddress objectForKey:@"City"], [dicAddress objectForKey:@"State"], [dicAddress objectForKey:@"ZIP"], [dicAddress objectForKey:@"Country"]]];
        [mkm addAnnotation:annotation];
    }];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if(![annotation isKindOfClass:[MKUserLocation class]]){
        MKAnnotationView *annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MKPinAnnotationView"];
        [annView setCanShowCallout:YES];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [btn addTarget:self action:@selector(selectLocation) forControlEvents:UIControlEventTouchUpInside];
        [annView setRightCalloutAccessoryView:btn];
        [annView setSelected:YES];
        return annView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    selectedAnn = view;
}

- (void)selectLocation{
    NSString *title = [[selectedAnn annotation] title];
    NSArray *arrSub = [[[selectedAnn annotation] subtitle] componentsSeparatedByString:@", "];
    NSString *addressOne = @"";
    NSString *addressTwo = @"";
    NSString *city = @"";
    NSString *state = @"";
    NSString *zip = @"";
    NSString *country = @"";
    addressOne = [arrSub objectAtIndex:0];
    if([arrSub count] > 4){
        addressTwo = [arrSub objectAtIndex:1];
        city = [arrSub objectAtIndex:2];
        NSArray *arrStateZip = [[arrSub objectAtIndex:3] componentsSeparatedByString:@"  "];
        state = [arrStateZip objectAtIndex:0];
        zip = [arrStateZip objectAtIndex:1];
        country = [arrSub objectAtIndex:4];
    }else if([arrSub count] > 3){
        city = [arrSub objectAtIndex:1];
        NSArray *arrStateZip = [[arrSub objectAtIndex:2] componentsSeparatedByString:@"  "];
        state = [arrStateZip objectAtIndex:0];
        zip = [arrStateZip objectAtIndex:1];
        country = [arrSub objectAtIndex:3];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Location Error" message:@"Invalid Address." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
        return;
    }
    NSDictionary *dic;
    if(!annotations){
        dic = @{@"title":title, @"addressOne":addressOne, @"addressTwo":addressTwo, @"city":city, @"state":state, @"zip":zip, @"country":country};
    }else{
        dic = @{@"title":title, @"addressOne":addressOne, @"addressTwo":addressTwo, @"city":city, @"state":state, @"zip":zip, @"country":country, @"annotations":annotations};
    }
    if(_fromMain){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MapAddressUser" object:dic];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MapAddressDealer" object:dic];
    }
    [self cancel];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self showLocationError:error];
}

- (void)showLocationError:(NSError *)error{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Location Error" message:[NSString stringWithFormat:@"%@ Please try again later.", [error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [annotations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MKPointAnnotation *annotation = [annotations objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [[cell layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[cell layer] setBorderWidth:1];
    [[cell layer] setCornerRadius:5];
    [cell setClipsToBounds:YES];
    [cell setBackgroundColor:[UIColor whiteColor]];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, tableView.frame.size.width-43, 20)];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:14]];
    [lblTitle setText:[annotation title]];
    [cell.contentView addSubview:lblTitle];
    UILabel *lblSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, tableView.frame.size.width-43, 20)];
    [lblSubTitle setFont:[UIFont systemFontOfSize:10]];
    [lblSubTitle setText:[annotation subtitle]];
    [cell.contentView addSubview:lblSubTitle];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [btn setTag:indexPath.row];
    [btn addTarget:self action:@selector(selectLocationByTable:) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(tableView.frame.size.width-33, 11, 22, 22)];
    [cell.contentView addSubview:btn];
    return cell;
}

- (void)selectLocationByTable:(UIButton *)btn{
    selectedAnn = [[MKPinAnnotationView alloc] initWithAnnotation:[annotations objectAtIndex:[btn tag]] reuseIdentifier:@"MKPinAnnotationView"];
    [self selectLocation];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MKPointAnnotation *annotation = [annotations objectAtIndex:indexPath.row];
    [mkm selectAnnotation:annotation animated:YES];
    MKCoordinateSpan smallSpan;
    smallSpan.latitudeDelta = .005;
    smallSpan.longitudeDelta = .005;
    currRegion.span = smallSpan;
    currRegion.center = annotation.coordinate;
    [mkm setRegion:currRegion animated:NO];
}

- (IBAction)cancel{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
}

@end
