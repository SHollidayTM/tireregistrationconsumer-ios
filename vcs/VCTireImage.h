//
//  VCTireImage.h
//  tireregconsumer-ipad
//
//  Created by Scott Holliday on 11/20/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCTireImage : UIViewController{
    IBOutlet UIImageView *imvNormal;
    IBOutlet UIImageView *imvWhite;
    IBOutlet UITextField *txtWhatYouSee;
    UIImage *imgNormal;
    UIImage *imgWhite;
}

- (void)setupImages:(NSDictionary *)dicImages;

@end
