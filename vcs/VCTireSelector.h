//
//  VCTireSelector.h
//  tireregconsumer-ipad
//
//  Created by Scott Holliday on 11/13/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBModule.h"

@interface VCTireSelector : UIViewController{
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnDone;
    NSArray *makes;
    NSArray *models;
    NSString *selectedMake;
    NSString *selectedModel;
}

- (IBAction)done;

@end
