//
//  VCSellerInfo.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "VCSelectByMap.h"

@interface VCSellerInfo : UIViewController <CLLocationManagerDelegate>{
    IBOutlet UITextField *txtCompanyName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPhone;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtAddressOne;
    IBOutlet UITextField *txtAddressTwo;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtZip;
    IBOutlet UIButton *btnAutoFill;
    IBOutlet UIButton *btnMapFill;
    IBOutlet UIButton *btnZipFill;
    IBOutlet UIView *vSavedSellers;
    IBOutlet UITableView *tbl;
    NSMutableArray *annotations;
    IBOutlet UIButton *btnDone;
    CLLocationManager *locMgr;
    VCSelectByMap *vcSelectByMap;
}

- (IBAction)autoFill;
- (IBAction)mapFill;
- (IBAction)zipFill;

@end
