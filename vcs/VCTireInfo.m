//
//  VCTireInfo.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/12/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCTireInfo.h"

@interface VCTireInfo ()

@end

@implementation VCTireInfo

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tireSelected:) name:@"TireSelected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnTIN:) name:@"ReturnTIN" object:nil];
    [[btnDone layer] setCornerRadius:5];
    [btnDone setClipsToBounds:YES];
    tires = [[NSMutableArray alloc] init];
    for(int i = 0; i < 4; i++){
        Tire *tire = [[Tire alloc] init];
        [tire setTin:@""];
        [tires addObject:tire];
    }
    NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
    [dFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *today = [dFormat stringFromDate:[NSDate date]];
    [txtPurchDate setText:today];
}

- (void)viewDidAppear:(BOOL)animated{
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        BOOL msgShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"vinDotMsgShown"];
        if(!msgShown){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"vinDotMsgShown"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowVinDotMsg" object:nil];
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@""]){
        return YES;
    }
    NSString *replace = [[textField text] stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
    replace = [replace stringByReplacingOccurrencesOfString:@"G" withString:@"6"];
    replace = [replace stringByReplacingOccurrencesOfString:@"I" withString:@"1"];
    replace = [replace stringByReplacingOccurrencesOfString:@"O" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Q" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"S" withString:@"5"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Z" withString:@"2"];
    NSString *tin = replace;
    Tire *tire = [[Tire alloc] init];
    [tire setTin:tin];
    [tires replaceObjectAtIndex:[textField tag] withObject:tire];
    [textField setText:replace];
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    Tire *tire = [[Tire alloc] init];
    [tire setTin:@""];
    [tires replaceObjectAtIndex:[textField tag] withObject:@""];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    if(![textField isEqual:txtPurchDate]){
        if([[textField text] length] > 5){
            NSString *tin = [textField text];
            NSRange range = NSMakeRange(0, 2);
            NSString *firstTwo = [tin substringWithRange:range];
            BOOL plantFound = NO;
            if([firstTwo isEqualToString:@"HP"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"FH"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"FJ"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"FK"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"FL"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"4M"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"B6"]){
                plantFound = YES;
            }else if([firstTwo isEqualToString:@"M3"]){
                plantFound = YES;
            }
            if(!plantFound){
                [textField setText:@""];
                UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"Your DOT code must begin with HP, FH, FH, FK, FL, 4M, B6, or M3." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [ctrAlert addAction:actOk];
                [self presentViewController:ctrAlert animated:YES completion:nil];
                return;
            }
            range = NSMakeRange([tin length]-4, 2);
            int week = [[tin substringWithRange:range] intValue];
            if((week < 1) || (week > 52)){
                [textField setText:@""];
                UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"The fourth and third from last digits in your DOT code must be between 01 and 52." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [ctrAlert addAction:actOk];
                [self presentViewController:ctrAlert animated:YES completion:nil];
                return;
            }
            range = NSMakeRange([tin length]-2, 2);
            int year = [[tin substringWithRange:range] intValue];
            if((year < 0) || (year > 16)){
                [textField setText:@""];
                UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"The last two digits in your DOT code must be between 00 and 16." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [ctrAlert addAction:actOk];
                [self presentViewController:ctrAlert animated:YES completion:nil];
                return;
            }
            currDot = (int)[textField tag];
            vcTireSelector = [[VCTireSelector alloc] initWithNibName:@"TireSelector" bundle:nil];
            [vcTireSelector.view setFrame:[[UIScreen mainScreen] bounds]];
            [[[[UIApplication sharedApplication] delegate] window] addSubview:vcTireSelector.view];
        }else{
            [textField setText:@""];
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"Your DOT code isn't long enough." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tires count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [[cell layer] setCornerRadius:5];
    [cell setClipsToBounds:YES];
    [cell setBackgroundColor:[UIColor whiteColor]];
    if(indexPath.row < [tires count]){
        Tire *tire = [tires objectAtIndex:indexPath.row];
        [[cell layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        [[cell layer] setBorderWidth:1];
        if(indexPath.row > 0){
            UIButton *btnCopy = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCopy setTag:indexPath.row];
            [btnCopy setFrame:CGRectMake(5, 17, 25, 25)];
            [btnCopy setBackgroundImage:[UIImage imageNamed:@"copy.png"] forState:UIControlStateNormal];
            [btnCopy addTarget:self action:@selector(copyTire:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnCopy];
        }
        UITextField *txtTIN = [[UITextField alloc] initWithFrame:CGRectMake(35, 5, tableView.frame.size.width-70, 29)];
        [txtTIN setTag:indexPath.row];
        [txtTIN setDelegate:self];
        [txtTIN setReturnKeyType:UIReturnKeyDone];
        [txtTIN setClearButtonMode:UITextFieldViewModeAlways];
        [txtTIN setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [txtTIN setAutocorrectionType:UITextAutocorrectionTypeNo];
        [txtTIN setBorderStyle:UITextBorderStyleRoundedRect];
        [txtTIN setBackgroundColor:[UIColor whiteColor]];
        [txtTIN setPlaceholder:[NSString stringWithFormat:@"DOT Code # %ld", (long)indexPath.row+1]];
        if([[tire tin] length] > 0){
            [txtTIN setText:[tire tin]];
        }
        [cell.contentView addSubview:txtTIN];
        UIButton *btnCamera = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCamera setTag:indexPath.row];
        [btnCamera setFrame:CGRectMake(tableView.frame.size.width-30, 16, 25, 25)];
        [btnCamera setBackgroundImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
        //  [btnCamera addTarget:self action:@selector(scanTIN:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnCamera];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(35, 32, tableView.frame.size.width-70, 26)];
        [lbl setTextColor:[UIColor grayColor]];
        if(([[tire make] length] > 0) && ([[tire model] length] > 0)){
            [lbl setText:[NSString stringWithFormat:@"%@ - %@", [tire make], [tire model]]];
        }
        [cell.contentView addSubview:lbl];
    }else{
        [cell setBackgroundColor:[UIColor clearColor]];
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAdd setFrame:CGRectMake((tableView.frame.size.width/2)-15, 5, 30, 30)];
        [btnAdd setBackgroundImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
        [btnAdd addTarget:self action:@selector(addTire:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnAdd];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [tires removeObjectAtIndex:indexPath.row];
        [tbl reloadData];
    }
}

- (void)copyTire:(UIButton *)sender{
    if([sender tag] > 0){
        NSString *tin = [tires objectAtIndex:[sender tag]-1];
        [tires replaceObjectAtIndex:[sender tag] withObject:tin];
        [tbl reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[sender tag] inSection:0]] withRowAnimation:YES];
    }
}

- (void)addTire:(UIButton *)sender{
    Tire *tire = [[Tire alloc] init];
    [tire setTin:@""];
    [tires addObject:tire];
    [tbl reloadData];
}

- (void)tireSelected:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    Tire *tire = [tires objectAtIndex:currDot];
    [tire setMake:[dic objectForKey:@"make"]];
    [tire setModel:[dic objectForKey:@"model"]];
    [tbl reloadData];
}

- (IBAction)showHelp{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowVinDotMsg" object:nil];
}

- (void)scanTIN:(UIButton *)sender{
    currDot = (int)[sender tag];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setAllowsEditing:YES];
    [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:picker animated:YES completion:NULL];
    }else{
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGRect rect = (CGRect){ CGPointZero, image.size };
    CGContextDrawImage(context, rect, image.CGImage);
    CGContextSetBlendMode(context, kCGBlendModeColorDodge);
    [[UIColor lightGrayColor] setFill];
    CGContextFillRect(context, rect);
    [[UIColor grayColor] setFill];
    CGContextFillRect(context, rect);
    [[UIColor blackColor] setFill];
    CGContextFillRect(context, rect);
    UIImage *whiteImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSDictionary *dicImages = @{@"normal":image, @"white":whiteImage};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowTireImage" object:dicImages];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)returnTIN:(NSNotification *)notification{
    Tire *tire = [tires objectAtIndex:currDot];
    [tire setTin:[notification object]];
    [tbl reloadData];
}

- (IBAction)done{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
}

@end
