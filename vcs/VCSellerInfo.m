//
//  VCSellerInfo.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCSellerInfo.h"

@interface VCSellerInfo ()

@end

@implementation VCSellerInfo

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mapAddressDealer:) name:@"MapAddressDealer" object:nil];
    [[btnAutoFill layer] setCornerRadius:5];
    [btnAutoFill setClipsToBounds:YES];
    [[btnMapFill layer] setCornerRadius:5];
    [btnMapFill setClipsToBounds:YES];
    [[btnZipFill layer] setCornerRadius:5];
    [btnZipFill setClipsToBounds:YES];
    [[vSavedSellers layer] setCornerRadius:5];
    [vSavedSellers setClipsToBounds:YES];
    [[btnDone layer] setCornerRadius:5];
    [btnDone setClipsToBounds:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtCompanyName]){
        [txtEmail becomeFirstResponder];
    }else if([textField isEqual:txtEmail]){
        [txtPhone becomeFirstResponder];
    }else if([textField isEqual:txtPhone]){
        [txtCountry becomeFirstResponder];
    }else if([textField isEqual:txtCountry]){
        [txtAddressOne becomeFirstResponder];
    }else if([textField isEqual:txtAddressOne]){
        [txtAddressTwo becomeFirstResponder];
    }else if([textField isEqual:txtAddressTwo]){
        [txtCity becomeFirstResponder];
    }else if([textField isEqual:txtCity]){
        [txtState becomeFirstResponder];
    }else if([textField isEqual:txtState]){
        [txtZip becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (IBAction)autoFill{
    [self setEditing:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:nil];
    locMgr = [[CLLocationManager alloc] init];
    [locMgr setDelegate:self];
    if([locMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locMgr requestWhenInUseAuthorization];
    }
    [locMgr startUpdatingLocation];
}

- (IBAction)mapFill{
    [self setEditing:NO];
    vcSelectByMap = [[VCSelectByMap alloc] initWithNibName:@"SelectByMap" bundle:nil];
    [vcSelectByMap setFromMain:NO];
    [vcSelectByMap setByZip:NO];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [vcSelectByMap.view setFrame:[[UIScreen mainScreen] bounds]];
        [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, vcSelectByMap.view.frame.size.height)];
        [[[[UIApplication sharedApplication] delegate] window] addSubview:vcSelectByMap.view];
    }else{
        [vcSelectByMap.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
        [self.view addSubview:vcSelectByMap.view];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    [manager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error){
        if(error){
            [self showLocationError:error];
            return;
        }
        NSDictionary *dicAddress = [[placemarks objectAtIndex:0] addressDictionary];
        [txtCountry setText:[dicAddress objectForKey:@"Country"]];
        [txtAddressOne setText:[dicAddress objectForKey:@"Name"]];
        if(![[dicAddress objectForKey:@"Street"] isEqualToString:[dicAddress objectForKey:@"Name"]]){
            [txtAddressTwo setText:[dicAddress objectForKey:@"Street"]];
        }
        [txtCity setText:[dicAddress objectForKey:@"City"]];
        [txtState setText:[dicAddress objectForKey:@"State"]];
        [txtZip setText:[dicAddress objectForKey:@"ZIP"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self showLocationError:error];
}

- (void)showLocationError:(NSError *)error{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Location Error" message:[NSString stringWithFormat:@"%@ Please try again later.", [error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)mapAddressDealer:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    [txtCompanyName setText:[dic objectForKey:@"title"]];
    [txtCountry setText:[dic objectForKey:@"country"]];
    [txtAddressOne setText:[dic objectForKey:@"addressOne"]];
    [txtAddressTwo setText:[dic objectForKey:@"addressTwo"]];
    [txtCity setText:[dic objectForKey:@"city"]];
    [txtState setText:[dic objectForKey:@"state"]];
    [txtZip setText:[dic objectForKey:@"zip"]];
    if([dic objectForKey:@"annotations"]){
        annotations = [dic objectForKey:@"annotations"];
    }else{
        annotations = nil;
    }
    [tbl reloadData];
}

- (IBAction)zipFill{
    [self setEditing:NO];
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Find Dealer" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [ctrAlert addTextFieldWithConfigurationHandler:^(UITextField *textField){
        [textField setPlaceholder:@"City, ST or Zip Code"];
    }];
    UIAlertAction *actCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        NSString *cityStateOrZip = [[[ctrAlert textFields] objectAtIndex:0] text];
        vcSelectByMap = [[VCSelectByMap alloc] initWithNibName:@"SelectByMap" bundle:nil];
        [vcSelectByMap setFromMain:NO];
        [vcSelectByMap setByZip:YES];
        [vcSelectByMap searchByCityStateOrZip:cityStateOrZip];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [vcSelectByMap.view setFrame:[[UIScreen mainScreen] bounds]];
            [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, vcSelectByMap.view.frame.size.height)];
            [[[[UIApplication sharedApplication] delegate] window] addSubview:vcSelectByMap.view];
        }else{
            [vcSelectByMap.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
            [self.view addSubview:vcSelectByMap.view];
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
        [UIView commitAnimations];
    }];
    [ctrAlert addAction:actCancel];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [annotations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MKPointAnnotation *annotation = [annotations objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [[cell layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[cell layer] setBorderWidth:1];
    [[cell layer] setCornerRadius:5];
    [cell setClipsToBounds:YES];
    [cell setBackgroundColor:[UIColor whiteColor]];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, tableView.frame.size.width-43, 20)];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:14]];
    [lblTitle setText:[annotation title]];
    [cell.contentView addSubview:lblTitle];
    UILabel *lblSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, tableView.frame.size.width-43, 20)];
    [lblSubTitle setFont:[UIFont systemFontOfSize:10]];
    [lblSubTitle setText:[annotation subtitle]];
    [cell.contentView addSubview:lblSubTitle];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MKPointAnnotation *annotation = [annotations objectAtIndex:indexPath.row];
    NSString *title = [annotation title];
    NSArray *arrSub = [[annotation subtitle] componentsSeparatedByString:@", "];
    NSString *addressOne = @"";
    NSString *addressTwo = @"";
    NSString *city = @"";
    NSString *state = @"";
    NSString *zip = @"";
    NSString *country = @"";
    addressOne = [arrSub objectAtIndex:0];
    if([arrSub count] > 4){
        addressTwo = [arrSub objectAtIndex:1];
        city = [arrSub objectAtIndex:2];
        NSArray *arrStateZip = [[arrSub objectAtIndex:3] componentsSeparatedByString:@"  "];
        state = [arrStateZip objectAtIndex:0];
        zip = [arrStateZip objectAtIndex:1];
        country = [arrSub objectAtIndex:4];
    }else if([arrSub count] > 3){
        city = [arrSub objectAtIndex:1];
        NSArray *arrStateZip = [[arrSub objectAtIndex:2] componentsSeparatedByString:@"  "];
        state = [arrStateZip objectAtIndex:0];
        zip = [arrStateZip objectAtIndex:1];
        country = [arrSub objectAtIndex:3];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Bad Address" message:@"The address for this location is improperly formatted. Please enter address manually." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
    [txtCompanyName setText:title];
    [txtCountry setText:country];
    [txtAddressOne setText:addressOne];
    [txtAddressTwo setText:addressTwo];
    [txtCity setText:city];
    [txtState setText:state];
    [txtZip setText:zip];
}

- (IBAction)done{
    //put info in user defaults?
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
}

@end
