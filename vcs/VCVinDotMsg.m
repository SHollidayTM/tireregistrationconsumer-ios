//
//  VCVinDotMsg.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/12/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCVinDotMsg.h"

@interface VCVinDotMsg ()

@end

@implementation VCVinDotMsg

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnOk layer] setCornerRadius:5];
    [btnOk setClipsToBounds:YES];
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (IBAction)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
