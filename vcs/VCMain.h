//
//  VCMain.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "VCSelectByMap.h"
#import "VCSellerInfo.h"
#import "VCVehicleInfo.h"
#import "VCTireInfo.h"
#import "VCVinDotMsg.h"
#import "VCTireImage.h"

@interface VCMain : UIViewController <CLLocationManagerDelegate>{
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPhone;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtAddressOne;
    IBOutlet UITextField *txtAddressTwo;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtZip;
    IBOutlet UIButton *btnAutoFill;
    IBOutlet UIButton *btnMapFill;
    IBOutlet UIView *vOptions;
    IBOutlet UIButton *btnSellerInfo;
    IBOutlet UIButton *btnVehicleInfo;
    IBOutlet UIButton *btnTireInfo;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIView *vLoading;
    CLLocationManager *locMgr;
    VCSelectByMap *vcSelectByMap;
    VCSellerInfo *vcSellerInfo;
    VCVehicleInfo *vcVehicleInfo;
    VCTireInfo *vcTireInfo;
    VCVinDotMsg *vcVinDotMsg;
    VCTireImage *vcTireImage;
    //iPad views
    IBOutlet UIView *vSellerHolder;
    IBOutlet UIView *vVehicleHolder;
    IBOutlet UIView *vTireHolder;
}

- (IBAction)autoFill;
- (IBAction)mapFill;
- (IBAction)sellerInfo;
- (IBAction)vehicleInfo;
- (IBAction)tireInfo;

@end
