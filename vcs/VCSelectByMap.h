//
//  VCSelectByMap.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface VCSelectByMap : UIViewController <MKMapViewDelegate>{
    IBOutlet UISearchBar *barSearch;
    IBOutlet MKMapView *mkm;
    BOOL userLocationShown;
    MKCoordinateRegion currRegion;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnDone;
    MKAnnotationView *selectedAnn;
    IBOutlet UIView *vListHolder;
    IBOutlet UITableView *tbl;
    MKCoordinateSpan span;
    NSMutableArray *annotations;
}

@property (assign) BOOL fromMain;
@property (assign) BOOL byZip;

- (void)searchByCityStateOrZip:(NSString *)cityStateOrZip;
- (IBAction)cancel;

@end
