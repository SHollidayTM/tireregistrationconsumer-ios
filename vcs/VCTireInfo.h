//
//  VCTireInfo.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/12/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tire.h"
#import "VCTireSelector.h"

@interface VCTireInfo : UIViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    IBOutlet UITextField *txtPurchDate;
    IBOutlet UITableView *tbl;
    NSMutableArray *tires;
    IBOutlet UIButton *btnDone;
    VCTireSelector *vcTireSelector;
    int currDot;
}

- (IBAction)showHelp;
- (IBAction)done;

@end
