//
//  VCTireSelector.m
//  tireregconsumer-ipad
//
//  Created by Scott Holliday on 11/13/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCTireSelector.h"

@interface VCTireSelector ()

@end

@implementation VCTireSelector

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnDone layer] setCornerRadius:5];
    [btnDone setClipsToBounds:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int count = 0;
    switch(component){
        case 0:
            makes = [[DBModule sharedInstance] getAllTireMakes];
            count = (int)[makes count];
            break;
        case 1:
            if(selectedMake){
                models = [[DBModule sharedInstance] getTireModelsByMake:selectedMake];
                count = (int)[models count];
            }
            break;
    }
    return count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString *title;
    switch(component){
        case 0:
            title = [makes objectAtIndex:row];
            break;
        case 1:
            title = [models objectAtIndex:row];
            break;
    }
    UILabel *lbl = [[UILabel alloc] init];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [lbl setFont:[UIFont systemFontOfSize:30]];
    }else{
        [lbl setFont:[UIFont systemFontOfSize:14]];
    }
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5]];
    [lbl setText:title];
    [[[pickerView subviews] objectAtIndex:1] setHidden:YES];
    [[[pickerView subviews] objectAtIndex:2] setHidden:YES];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch(component){
        case 0:
            selectedMake = [makes objectAtIndex:row];
            models = [[DBModule sharedInstance] getTireModelsByMake:selectedMake];
            [pck reloadAllComponents];
            break;
        case 1:
            selectedModel = [models objectAtIndex:row];
            break;
    }
}

- (IBAction)done{
    if(([selectedMake length] > 0) && ([selectedModel length] > 0)){
        NSDictionary *dic = @{@"make":selectedMake, @"model":selectedModel};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TireSelected" object:dic];
        [self.view removeFromSuperview];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Make Selection" message:@"Please select your tire Brand and Model." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

@end
