//
//  VCTireImage.m
//  tireregconsumer-ipad
//
//  Created by Scott Holliday on 11/20/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCTireImage.h"

@interface VCTireImage ()

@end

@implementation VCTireImage

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
    [imvNormal setImage:imgNormal];
    [imvWhite setImage:imgWhite];
    [txtWhatYouSee becomeFirstResponder];
}

- (void)setupImages:(NSDictionary *)dicImages{
    imgNormal = [dicImages objectForKey:@"normal"];
    imgWhite = [dicImages objectForKey:@"white"];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@""]){
        return YES;
    }
    NSString *replace = [[textField text] stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
    replace = [replace stringByReplacingOccurrencesOfString:@"G" withString:@"6"];
    replace = [replace stringByReplacingOccurrencesOfString:@"I" withString:@"1"];
    replace = [replace stringByReplacingOccurrencesOfString:@"O" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Q" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"S" withString:@"5"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Z" withString:@"2"];
    [txtWhatYouSee setText:replace];
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    if([[textField text] length] > 5){
        NSString *tin = [textField text];
        NSRange range = NSMakeRange(0, 2);
        NSString *firstTwo = [tin substringWithRange:range];
        BOOL plantFound = NO;
        if([firstTwo isEqualToString:@"HP"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"FH"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"FJ"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"FK"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"FL"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"4M"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"B6"]){
            plantFound = YES;
        }else if([firstTwo isEqualToString:@"M3"]){
            plantFound = YES;
        }
        if(!plantFound){
            [textField setText:@""];
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"Your DOT code must begin with HP, FH, FH, FK, FL, 4M, B6, or M3." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            return;
        }
        range = NSMakeRange([tin length]-4, 2);
        int week = [[tin substringWithRange:range] intValue];
        if((week < 1) || (week > 52)){
            [textField setText:@""];
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"The fourth and third from last digits in your DOT code must be between 01 and 52." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            return;
        }
        range = NSMakeRange([tin length]-2, 2);
        int year = [[tin substringWithRange:range] intValue];
        if((year < 0) || (year > 16)){
            [textField setText:@""];
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"The last two digits in your DOT code must be between 00 and 16." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            return;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnTIN" object:[txtWhatYouSee text]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [textField setText:@""];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Invalid DOT Code" message:@"Your DOT code isn't long enough." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

@end
