//
//  VCVehicleInfo.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/11/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCVehicleInfo.h"

@interface VCVehicleInfo ()

@end

@implementation VCVehicleInfo

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnDone layer] setCornerRadius:5];
    [btnDone setClipsToBounds:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        BOOL msgShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"vinDotMsgShown"];
        if(!msgShown){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"vinDotMsgShown"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowVinDotMsg" object:nil];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@""]){
        return YES;
    }
    NSString *replace = [[textField text] stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
    replace = [replace stringByReplacingOccurrencesOfString:@"G" withString:@"6"];
    replace = [replace stringByReplacingOccurrencesOfString:@"I" withString:@"1"];
    replace = [replace stringByReplacingOccurrencesOfString:@"O" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Q" withString:@"0"];
    replace = [replace stringByReplacingOccurrencesOfString:@"S" withString:@"5"];
    replace = [replace stringByReplacingOccurrencesOfString:@"Z" withString:@"2"];
    [textField setText:replace];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    //instead find ymm for vin
}

- (IBAction)scanVIN{
    [txtVIN resignFirstResponder];
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if(!input){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device does not support VIN scan." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        [vVidBlock setHidden:NO];
        session = [[AVCaptureSession alloc] init];
        [session addInput:input];
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [session addOutput:captureMetadataOutput];
        dispatch_queue_t dispatch = dispatch_queue_create("queue", NULL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch];
        [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
        videoLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
        [videoLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [videoLayer setFrame:[[UIScreen mainScreen] bounds]];
        btnCancelScan = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCancelScan setBackgroundImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
        [btnCancelScan addTarget:self action:@selector(cancelScan) forControlEvents:UIControlEventTouchUpInside];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [btnCancelScan setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-20, [UIScreen mainScreen].bounds.size.height-50, 40, 40)];
            [[[[UIApplication sharedApplication] delegate] window] addSubview:btnCancelScan];
            [[[[UIApplication sharedApplication] delegate] window].layer insertSublayer:videoLayer below:btnCancelScan.layer];
        }else{
            [btnCancelScan setFrame:CGRectMake((self.view.frame.size.width/2)-20, self.view.frame.size.height-50, 40, 40)];
            [self.view addSubview:btnCancelScan];
            [self.view.layer insertSublayer:videoLayer below:btnCancelScan.layer];
        }
        [session startRunning];
    }
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if(metadataObjects != nil && [metadataObjects count] > 0){
        [vVidBlock setHidden:YES];
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        [txtVIN performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
        [self cancelScan];
    }
}

- (void)cancelScan{
    [btnCancelScan removeFromSuperview];
    [session stopRunning];
    [videoLayer removeFromSuperlayer];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int count = 0;
    switch(component){
        case 0:
            years = [[DBModule sharedInstance] getAllYears];
            count = (int)[years count];
            break;
        case 1:
            if(selectedYear > 0){
                makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
                count = (int)[makes count];
            }
            break;
        case 2:
            if(selectedMake){
                models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
                count = (int)[models count];
            }
            break;
    }
    return count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString *title;
    switch(component){
        case 0:
            if([[years objectAtIndex:row] isEqual:[NSNumber numberWithInt:0]]){
                title = @"";
            }else{
                title = [[years objectAtIndex:row] stringValue];
            }
            break;
        case 1:{
            Make *make = [makes objectAtIndex:row];
            title = [make name];
        }
            break;
        case 2:{
            Model *model = [models objectAtIndex:row];
            title = [model name];
        }
            break;
    }
    UILabel *lbl = [[UILabel alloc] init];
    [lbl setFont:[UIFont systemFontOfSize:14]];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5]];
    [lbl setText:title];
    [[[pickerView subviews] objectAtIndex:1] setHidden:YES];
    [[[pickerView subviews] objectAtIndex:2] setHidden:YES];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [txtVIN setText:@""];
    [txtVIN resignFirstResponder];
    switch(component){
        case 0:
            selectedYear = [years objectAtIndex:row];
            makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
            if([pickerView selectedRowInComponent:1] < [makes count]){
                selectedMake = [makes objectAtIndex:[pickerView selectedRowInComponent:1]];
            }else{
                selectedMake = [makes lastObject];
            }
            models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
            if([pickerView selectedRowInComponent:2] < [models count]){
                selectedModel = [models objectAtIndex:[pickerView selectedRowInComponent:2]];
            }else{
                selectedModel = [models lastObject];
            }
            [pck reloadAllComponents];
            break;
        case 1:
            selectedMake = [makes objectAtIndex:row];
            [pck reloadAllComponents];
            break;
        case 2:
            selectedModel = [models objectAtIndex:row];
            break;
    }
}

- (IBAction)done{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
}

@end
