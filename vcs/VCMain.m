//
//  VCMain.m
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/10/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCMain.h"

@interface VCMain ()

@end

@implementation VCMain

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLoading) name:@"StartLoading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoading) name:@"StopLoading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mapAddressUser:) name:@"MapAddressUser" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showVinDotMsg) name:@"ShowVinDotMsg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTireImage:) name:@"ShowTireImage" object:nil];
    [[btnAutoFill layer] setCornerRadius:5];
    [btnAutoFill setClipsToBounds:YES];
    [[btnMapFill layer] setCornerRadius:5];
    [btnMapFill setClipsToBounds:YES];
    [[btnSubmit layer] setCornerRadius:5];
    [btnSubmit setClipsToBounds:YES];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        vcSellerInfo = [[VCSellerInfo alloc] initWithNibName:@"SellerInfo" bundle:nil];
        [vcSellerInfo.view setFrame:CGRectMake(0, 0, vSellerHolder.frame.size.width, vSellerHolder.frame.size.height)];
        [vSellerHolder addSubview:vcSellerInfo.view];
        vcVehicleInfo = [[VCVehicleInfo alloc] initWithNibName:@"VehicleInfo" bundle:nil];
        [vcVehicleInfo.view setFrame:CGRectMake(0, 0, vVehicleHolder.frame.size.width, vVehicleHolder.frame.size.height)];
        [vVehicleHolder addSubview:vcVehicleInfo.view];
        vcTireInfo = [[VCTireInfo alloc] initWithNibName:@"TireInfo" bundle:nil];
        [vcTireInfo.view setFrame:CGRectMake(0, 0, vTireHolder.frame.size.width, vTireHolder.frame.size.height)];
        [vTireHolder addSubview:vcTireInfo.view];
    }else{
        [[vOptions layer] setCornerRadius:5];
        [vOptions setClipsToBounds:YES];
        [[btnSellerInfo layer] setCornerRadius:5];
        [btnSellerInfo setClipsToBounds:YES];
        [[btnVehicleInfo layer] setCornerRadius:5];
        [btnVehicleInfo setClipsToBounds:YES];
        [[btnTireInfo layer] setCornerRadius:5];
        [btnTireInfo setClipsToBounds:YES];
    }
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidAppear:(BOOL)animated{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        BOOL msgShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"vinDotMsgShown"];
        if(!msgShown){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"vinDotMsgShown"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowVinDotMsg" object:nil];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtFirstName]){
        [txtLastName becomeFirstResponder];
    }else if([textField isEqual:txtLastName]){
        [txtEmail becomeFirstResponder];
    }else if([textField isEqual:txtEmail]){
        [txtPhone becomeFirstResponder];
    }else if([textField isEqual:txtPhone]){
        [txtCountry becomeFirstResponder];
    }else if([textField isEqual:txtCountry]){
        [txtAddressOne becomeFirstResponder];
    }else if([textField isEqual:txtAddressOne]){
        [txtAddressTwo becomeFirstResponder];
    }else if([textField isEqual:txtAddressTwo]){
        [txtCity becomeFirstResponder];
    }else if([textField isEqual:txtCity]){
        [txtState becomeFirstResponder];
    }else if([textField isEqual:txtState]){
        [txtZip becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (IBAction)autoFill{
    [self setEditing:NO];
    [self startLoading];
    locMgr = [[CLLocationManager alloc] init];
    [locMgr setDelegate:self];
    if([locMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locMgr requestWhenInUseAuthorization];
    }
    [locMgr startUpdatingLocation];
}

- (IBAction)mapFill{
    [self setEditing:NO];
    if(!vcSelectByMap){
        vcSelectByMap = [[VCSelectByMap alloc] initWithNibName:@"SelectByMap" bundle:nil];
        [vcSelectByMap setFromMain:YES];
        [vcSelectByMap setByZip:NO];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [vcSelectByMap.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
        }else{
            [vcSelectByMap.view setFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
            [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height-100)];
        }
        [self.view addSubview:vcSelectByMap.view];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcSelectByMap.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    [manager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error){
        if(error){
            [self showLocationError:error];
            return;
        }
        NSDictionary *dicAddress = [[placemarks objectAtIndex:0] addressDictionary];
        [txtCountry setText:[dicAddress objectForKey:@"Country"]];
        [txtAddressOne setText:[dicAddress objectForKey:@"Name"]];
        if(![[dicAddress objectForKey:@"Street"] isEqualToString:[dicAddress objectForKey:@"Name"]]){
            [txtAddressTwo setText:[dicAddress objectForKey:@"Street"]];
        }
        [txtCity setText:[dicAddress objectForKey:@"City"]];
        [txtState setText:[dicAddress objectForKey:@"State"]];
        [txtZip setText:[dicAddress objectForKey:@"ZIP"]];
        [self stopLoading];
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self showLocationError:error];
}

- (void)showLocationError:(NSError *)error{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Location Error" message:[NSString stringWithFormat:@"%@ Please try again later.", [error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (IBAction)sellerInfo{
    [self setEditing:NO];
    if(!vcSellerInfo){
        vcSellerInfo = [[VCSellerInfo alloc] initWithNibName:@"SellerInfo" bundle:nil];
        [vcSellerInfo.view setFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
        [vcSellerInfo.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height-100)];
        [self.view insertSubview:vcSellerInfo.view belowSubview:vLoading];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcSellerInfo.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)mapAddressUser:(NSNotification *)notification{
    NSDictionary *dicAddress = [notification object];
    [txtCountry setText:[dicAddress objectForKey:@"country"]];
    [txtAddressOne setText:[dicAddress objectForKey:@"addressOne"]];
    [txtAddressTwo setText:[dicAddress objectForKey:@"addressTwo"]];
    [txtCity setText:[dicAddress objectForKey:@"city"]];
    [txtState setText:[dicAddress objectForKey:@"state"]];
    [txtZip setText:[dicAddress objectForKey:@"zip"]];
}

- (IBAction)vehicleInfo{
    [self setEditing:NO];
    if(!vcVehicleInfo){
        vcVehicleInfo = [[VCVehicleInfo alloc] initWithNibName:@"VehicleInfo" bundle:nil];
        [vcVehicleInfo.view setFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
        [vcVehicleInfo.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height-100)];
        [self.view insertSubview:vcVehicleInfo.view belowSubview:vLoading];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcVehicleInfo.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (IBAction)tireInfo{
    [self setEditing:NO];
    if(!vcTireInfo){
        vcTireInfo = [[VCTireInfo alloc] initWithNibName:@"TireInfo" bundle:nil];
        [vcTireInfo.view setFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100)];
        [vcTireInfo.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height-100)];
        [self.view insertSubview:vcTireInfo.view belowSubview:vLoading];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcTireInfo.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)startLoading{
    [vLoading setHidden:NO];
}

- (void)stopLoading{
    [vLoading setHidden:YES];
}

- (void)showVinDotMsg{
    vcVinDotMsg = [[VCVinDotMsg alloc] initWithNibName:@"VinDotMsg" bundle:nil];
    [self presentViewController:vcVinDotMsg animated:YES completion:nil];
}

- (void)showTireImage:(NSNotification *)notification{
    vcTireImage = [[VCTireImage alloc] initWithNibName:@"TireImage" bundle:nil];
    [self presentViewController:vcTireImage animated:YES completion:nil];
    [vcTireImage setupImages:[notification object]];
}

@end
