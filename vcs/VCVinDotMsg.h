//
//  VCVinDotMsg.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/12/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCVinDotMsg : UIViewController{
    IBOutlet UIButton *btnOk;
}

- (IBAction)dismiss;

@end
