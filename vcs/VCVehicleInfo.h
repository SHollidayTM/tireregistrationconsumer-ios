//
//  VCVehicleInfo.h
//  tireregconsumer-iphone
//
//  Created by Scott Holliday on 11/11/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "DBModule.h"

@interface VCVehicleInfo : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>{
    IBOutlet UITextField *txtVIN;
    IBOutlet UIPickerView *pck;
    IBOutlet UIView *vVidBlock;
    UIButton *btnCancelScan;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *videoLayer;
    NSString *selectedYear;
    Make *selectedMake;
    Model *selectedModel;
    NSArray *years;
    NSArray *makes;
    NSArray *models;
    BOOL byVIN;
    IBOutlet UIButton *btnDone;
}

- (IBAction)scanVIN;
- (IBAction)done;

@end
