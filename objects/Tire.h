//
//  Tire.h
//  tireregconsumer-ipad
//
//  Created by Scott Holliday on 11/13/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tire : NSObject

@property (nonatomic, strong) NSString *tin;
@property (nonatomic, strong) NSString *make;
@property (nonatomic, strong) NSString *model;

@end
